package com.waes.assignment.controllers;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.Base64;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestInstance(PER_CLASS)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
@ContextConfiguration
@WebAppConfiguration
public class DiffControllerIntegrationTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    private String dataLeft;
    private String dataRight;

    @BeforeAll
    public void setup() {

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        dataLeft = Base64.getEncoder().encodeToString("QUJDREU=".getBytes());
        dataRight = Base64.getEncoder().encodeToString("QUNDREU=".getBytes());
    }

    @Test
    public void givenWac_whenServletContext_thenItProvidesDiffController() {
        ServletContext servletContext = wac.getServletContext();
        assertNotNull(servletContext);
        assertTrue(servletContext instanceof MockServletContext);
        assertNotNull(wac.getBean("diffController"));
    }

    @Test
    public void ifSendProperLeftData_ShouldStoreItToDbAndReturn201StatusCode() throws Exception {
        mockMvc.perform(post("/api/v1/diff/1/left")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"data\":\""+dataLeft+"\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/diff/1")));
    }

    @Test
    public void ifSendProperRightData_ShouldStoreItToDbAndReturn201StatusCode() throws Exception {
        mockMvc.perform(post("/api/v1/diff/1/right")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"data\":\""+dataLeft+"\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/diff/1")));
    }

    @Test
    public void ifProvideEqualsLeftAndRightDataAndGetDiff_shouldReturn204StatusAndEmptyContent() throws Exception {

        //Set Left data
        mockMvc.perform(post("/api/v1/diff/2/left")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"data\":\""+dataLeft+"\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/diff/2")));

        //Set the same data for right
        mockMvc.perform(post("/api/v1/diff/2/right")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"data\":\""+dataLeft+"\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/diff/2")));

        //Diff left and right
        mockMvc.perform(get("/api/v1/diff/2"))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }

    @Test
    public void ifProvideNotEqualsLeftAndRightDataAndGetDiff_shouldReturn200StatusAndDiffData() throws Exception {

        //Set left data
        mockMvc.perform(post("/api/v1/diff/3/left")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"data\":\""+dataLeft+"\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/diff/3")));

        //Set right data
        mockMvc.perform(post("/api/v1/diff/3/right")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"data\":\""+dataRight+"\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/diff/3")));

        //Diff left and right
        mockMvc.perform(get("/api/v1/diff/3"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].startPosition").value(2))
                .andExpect(jsonPath("$[0].offset").value(1));
    }

}
