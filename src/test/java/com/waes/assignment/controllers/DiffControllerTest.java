package com.waes.assignment.controllers;

import com.waes.assignment.models.Data;
import com.waes.assignment.models.dto.DiffDto;
import com.waes.assignment.services.DiffService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestInstance(PER_CLASS)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
class DiffControllerTest {
    @MockBean
    private DiffService service;
    private MockMvc mockMvc;
    private Data data = Data.builder().id(1L).dataId(1L).data("test").build();

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(new DiffController(service)).build();
        when(service.add(any(Data.class))).thenReturn(Optional.of(data));
    }

    @Test
    public void ifProvidedAllRequiredParametersForLeftEndpoint_ShouldAddLeftData() throws Exception {
        mockMvc.perform(post("/api/v1/diff/1/left")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"data\":\"test\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/diff/1")));
    }

    @Test
    public void ifSendRequestWithoutBodyForLeftEndpoint_ShouldReturnBadRequestError() throws Exception {
        mockMvc.perform(post("/api/v1/diff/1/left"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void ifProvidedBadContentTypeForLeftEndpoint_ShouldReturnUnsupportedMediaTypeError() throws Exception {
        mockMvc.perform(post("/api/v1/diff/1/left")
                .contentType(MediaType.APPLICATION_XML)
                .characterEncoding("utf-8")
                .content("{\"data\":\"test\"}"))
                .andDo(print());
    }

    @Test
    public void ifProvidedAllRequiredParametersForRightEndpoint_ShouldAddRightData() throws Exception {
        mockMvc.perform(post("/api/v1/diff/1/right")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"data\":\"test\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/diff/1")));
    }

    @Test
    public void ifSendRequestWithoutBodyForRightEndpoint_ShouldReturnBadRequestError() throws Exception {
        mockMvc.perform(post("/api/v1/diff/1/right"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void ifProvidedBadContentTypeForRightEndpoint_ShouldReturnUnsupportedMediaTypeError() throws Exception {
        mockMvc.perform(post("/api/v1/diff/1/right")
                .contentType(MediaType.APPLICATION_XML)
                .characterEncoding("utf-8")
                .content("{\"data\":\"test\"}"))
                .andDo(print());
    }

    @Test
    public void ifLeftAndRightNotEquals_shouldReturnResponseDiffsData() throws Exception {
        Data left = Data.builder().id(1L).dataId(5L).data("QUJDREU=").build();
        Data right = Data.builder().id(2L).dataId(5L).data("QUNDREU=").build();
        List<Data> datas = Arrays.asList(left, right);
        List<DiffDto> diffDtos = Arrays.asList(new DiffDto(1,2));

        when(service.getData(5L)).thenReturn(datas);
        when(service.diff(any(byte[].class), any(byte[].class))).thenReturn(diffDtos);

        mockMvc.perform(get("/api/v1/diff/5"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].startPosition").value(1))
                .andExpect(jsonPath("$[0].offset").value(2));
    }

    @Test
    public void ifLeftAndRightEquals_shouldReturnOkResponseWithEmptyContent() throws Exception {
        Data left = Data.builder().id(1L).dataId(5L).data("QUJDREU=").build();
        Data right = Data.builder().id(2L).dataId(5L).data("QUJDREU=").build();
        List<Data> datas = Arrays.asList(left, right);

        when(service.getData(5L)).thenReturn(datas);
        when(service.diff(any(byte[].class), any(byte[].class))).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/api/v1/diff/5"))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }

    @Test
    public void ifProvidedDataIdDoesNotExists_shouldReturnNotFoundError() throws Exception {
        when(service.getData(999L)).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/api/v1/diff/999"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
