package com.waes.assignment.services;

import com.waes.assignment.models.Data;
import com.waes.assignment.models.dto.DiffDto;
import com.waes.assignment.repositories.DataRepository;
import com.waes.assignment.services.impl.DiffServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@TestInstance(PER_CLASS)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class DiffServiceTest {
    @MockBean
    private DataRepository repository;
    private DiffService service;

    private Data data = Data.builder().id(1L).dataId(1L).data("test").build();
    private List<Data> datas = Arrays.asList(Data.builder().id(1L).dataId(1L).data("left").build(),
            Data.builder().id(2L).dataId(1L).data("right").build());

    @BeforeEach
    public void init() {
        service = new DiffServiceImpl(repository);

        when(repository.save(any(Data.class))).thenReturn(data);
        when(repository.findByDataId(anyLong())).thenReturn(datas);
        Mockito.lenient().when(repository.findByDataId(999L)).thenReturn(Collections.emptyList());
    }

    @Test
    public void ifAddDataWithoutRequiredParameter_shouldThrowNullPointerException() {
        assertThrows(NullPointerException.class,
                ()->service.add(null));
    }

    @Test
    public void ifAddDataWithProperParameter_shouldReturnSavedDataObject() {
        Optional<Data> expectedObject = Optional.of(data);
        assertEquals(expectedObject, service.add(data));
    }

    @Test
    public void ifGetDataAndMissRequiredIdParameter_shouldThrowNullPointerException() {
        assertThrows(NullPointerException.class,
                ()->service.getData(null));
    }

    @Test
    public void ifGetDataWithProperIdParameter_shouldReturnDataList() {
        assertEquals(datas,service.getData(1L));
    }

    @Test
    public void ifGetDataAndWithNotExistsDataId_shouldReturnEmptyList() {
        assertTrue(service.getData(999L).isEmpty());

    }

    @Test
    public void ifGettingDataWithProvidedParameter_shouldBeDoneOneRepositoryCall() {
        service.getData(1L);
        verify(repository, times(1)).findByDataId(1L);
    }

    @Test
    public void ifDataHasNotEqualsSize_shouldReturnEmptyList() {
        byte[] left = "QUJDREU=".getBytes();
        byte[] right = "QUJDREUE=".getBytes();
        assertTrue(service.diff(left, right).isEmpty());
    }

    @Test
    public void ifLeftAndRightIsNotEquals_shouldReturnDiffData() {
        byte[] left = "QUJDREU=".getBytes();
        byte[] right = "QUNDREU=".getBytes();
        List<DiffDto> expected = Arrays.asList(new DiffDto(2,1));
        assertEquals(expected, service.diff(left, right));
    }

    @Test
    public void ifLeftAndRightIsEquals_shouldReturnEmptyDiffData() {
        byte[] left = "QUJDREU=".getBytes();
        byte[] right = "QUJDREU=".getBytes();
        List<DiffDto> expected = Collections.emptyList();
        assertEquals(expected, service.diff(left, right));
    }
}
