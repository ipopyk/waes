package com.waes.assignment.controllers;

import com.waes.assignment.models.Data;
import com.waes.assignment.models.dto.DiffDto;
import com.waes.assignment.models.dto.DataDto;
import com.waes.assignment.services.DiffService;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

/**
 *
 * Endpoints:
 * /api/v1/diff/{id}/left - set left data
 * /api/v1/diff/{id}/right - set right data
 * /api/v1/diff/{id} - diff left and right
 *
 */
@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE)
public class DiffController {
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    private final DiffService service;

    /**
     * @param diffService
     */
    public DiffController(DiffService diffService) {
        service = diffService;
    }

    /**
     * @param id - data id
     * @param dataDto - data object
     * @return 201 Created or 400 Bad Request
     */
    @PostMapping(value = "/diff/{id}/left")
    public ResponseEntity left(@PathVariable Long id,
                                @Valid @RequestBody DataDto dataDto) {
        return service.add(buildData(id, dataDto))
                .map(data -> ResponseEntity.created(URI.create("/api/v1/diff/" + data.getDataId())).build())
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * @param id - data id
     * @param dataDto - data object
     * @return 201 Created or 400 Bad Request
     */
    @PostMapping(value = "/diff/{id}/right")
    public ResponseEntity right(@PathVariable Long id,
                                @Valid @RequestBody DataDto dataDto) {
        return service.add(buildData(id, dataDto))
                .map(data -> ResponseEntity.created(URI.create("/api/v1/diff/" + data.getDataId())).build())
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * @param id - data id
     * @return 200 OK, 204 No Content or 404 Not Found
     */
    @GetMapping(value = "/diff/{id}")
    public ResponseEntity<List<DiffDto>> diff(@PathVariable Long id) {
        List<Data> data = service.getData(id);
        if (data.isEmpty()) {
            //If no data for provided ID return 404 Not Found
            return ResponseEntity.notFound().build();
        }
        byte[] left = decode(data.get(LEFT).getData());
        byte[] right = decode(data.get(RIGHT).getData());
        List<DiffDto> diffDtos = service.diff(left, right);
        if (diffDtos.isEmpty()) {
            //If left and right equals or have different length return 204 No Content
            return ResponseEntity.noContent().build();
        } else {
            //If there are diffs return 200 OK and diff object in response body
            return ResponseEntity.ok(diffDtos);
        }
    }

    /**
     * @param id
     * @param dataDto
     * @return Data object
     *
     * Build and return Data object
     *
     */
    private Data buildData(Long id, DataDto dataDto) {
        return Data.builder().dataId(id).data(dataDto.getData()).build();
    }

    /**
     * @param data
     * @return decoed byte data
     *
     * Decode data and return byte array
     *
     */
    private byte[] decode(String data) {
        Objects.requireNonNull(data);
        return Base64.getDecoder().decode(data);
    }
}
