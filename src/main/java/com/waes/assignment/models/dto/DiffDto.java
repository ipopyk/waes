package com.waes.assignment.models.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiffDto {
    private Integer startPosition;
    private Integer offset;
}
