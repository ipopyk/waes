package com.waes.assignment.models.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataDto {
    @NotEmpty(message = "Required parameter should be present")
    private String data;
}
