package com.waes.assignment.services;

import com.waes.assignment.models.Data;
import com.waes.assignment.models.dto.DiffDto;

import java.util.List;
import java.util.Optional;

public interface DiffService {
    /**
     * @param data
     * @return Optional<Data>
     *
     *     Store left or right data into DB
     *
     */
    Optional<Data> add(Data data);

    /**
     * @param id
     * @return List<Data>
     *
     *     Get list of left and right datas by DATA_ID
     *
     */
    List<Data> getData(Long id);

    /**
     * @param left
     * @param right
     * @return List<DiffDto>
     *
     *  Compare left and right and return list of diff datas
     *
     *  if left and right are equals or have not equals size return empty list
     *
     */
    List<DiffDto> diff(byte[] left, byte[] right);
}
