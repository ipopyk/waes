package com.waes.assignment.services.impl;

import com.waes.assignment.models.Data;
import com.waes.assignment.models.dto.DiffDto;
import com.waes.assignment.repositories.DataRepository;
import com.waes.assignment.services.DiffService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DiffServiceImpl implements DiffService {
    private final DataRepository repository;

    public DiffServiceImpl(DataRepository repository) {
        this.repository = Objects.requireNonNull(repository);
    }

    @Override
    public Optional<Data> add(Data data) {
        Objects.requireNonNull(data);
        return Optional.of(repository.save(data));
    }

    @Override
    public List<Data> getData(Long id) {
        Objects.requireNonNull(id);
        return repository.findByDataId(id);
    }

    @Override
    public List<DiffDto> diff(byte[] left, byte[] right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        List<DiffDto> diffDtos = new ArrayList<>();
        int leftLength = left.length;
        int rightLength = right.length;
        if (leftLength != rightLength) {
            //if right and legt have diff length
            return Collections.emptyList();
        }
        int startPosition = -1;
        int offset = 0;
        for (int i=0; i<leftLength; i++) {
            if (left[i] != right[i]) {
                if (startPosition == -1) {
                    //start diff position
                    startPosition = i;
                }
                offset++;
            } else {
                if (startPosition != -1) {
                    //add diff data
                    diffDtos.add(new DiffDto(startPosition, offset));
                    //reset diff value
                    startPosition = -1;
                    offset = 0;
                }
            }
        }
        //if whole object is diff
        if (startPosition != -1) {
            diffDtos.add(new DiffDto(startPosition, offset));
        }
        return Collections.unmodifiableList(diffDtos);
    }
}
