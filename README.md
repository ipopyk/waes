## The assignment
1. Provide 2 http endpoints that accepts JSON base64 encoded binary data on both endpoints
    +  <host>/v1/diff/<ID>/left and <host>/v1/diff/<ID>/right
2. The provided data needs to be diff-ed and the results shall be available on a third end point
    +  <host>/v1/diff/<ID>
3. The results shall provide the following info in JSON format
    +  If equal return that
    +  If not of equal size just return that
    +  If of same size provide insight in where the diffs are, actual diffs are not needed.
        +  So mainly offsets + length in the data
4. Make assumptions in the implementation explicit, choices are good but need to be communicated

## Tech stack
+ Java 11
+ Spring-boot 2
+ H2 DB
+ JUnit 5
+ Mockito
+ lombok
+ Swagger

## Requirement
1. Java
2. Maven
3. Port 8080 should be open
4. App use default credentials for H2 DB

## Run tests
```bash
mvn clean test
```

## Run app
```bash
mvn spring-boot:run
```
App will be run on port 8080.

URL: http://localhost:8080

## Rest API Docs
+ [Swagger docs](http://localhost:8080/swagger-ui.html)

## Improvements
+ Add caching
+ Add more validation for provided data (check if given data id exists in db)